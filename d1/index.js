const  express = require('express');
const mongoose = require("mongoose");
const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://josephjconcillado:dBOpsesYrUx0H4ib@wdc028-course-booking.i1zei0q.mongodb.net/b190-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=> console.log("We're connected to the database"));

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

// SECTION - models
// models in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// Models use schemas and they act as the middleman from the server to the database

const Task = mongoose.model("Task", taskSchema);


app.use(express.json());
app.use(express.urlencoded({extended:true}));
/**
 * BUSINESS LOGIC
 *  add a functionality that will check if there are duplicate tasks
 *      -if the task is already existing, we ruturn an error
 *      -if task is not existing, we did it in the database
 *  the task will be sent from the request body
 *  create a new task object with a "name" field/property
 *  the "status" property does not need to be provided becuase our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks",(req,res)=>{
    Task.findOne({name: req.body.name},(err,result) => {
        if(result !== null && result.name === req.body.name){
            return res.send("Duplicate task found");
        }else{
            let newTask = new Task({
                name: req.body.name
            });
            newTask.save((saveErr,savedTask) => {
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return res.status(201).send("New task created");
                }
            });
        };
    });
});

app.get("/tasks",(req,res)=>{
    Task.find({},(err,result)=>{
        if(err){
            return console.log(err);
        }else{
            res.send(result);
        }
    });
});

// app.get("/tasks",(req,res)=>{
// 	Task.find({}, (err,result) => {
// 		if(result === null){
// 			return res.send("Error")
// 		}else{
// 			res.send(result)
// 		}
// 	})
// })

// ACTIVITY CODE
// 1. Create a User schema.


const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

// 2. Create a User model.
const User = mongoose.model("User", userSchema);

// 3. Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup",(req,res)=>{
    User.findOne({username: req.body.username},(err,result) => {
        if(err){
            console.log(err);
        }
        if(result !== null && result.username === req.body.username){
            res.send("Username is already registered!");
        }else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save((saveErr,savedUser) => {
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return res.status(201).send("New user registered.");
                }
            });
        };
    });
});
















app.listen(port,() => console.log(`Server running at port: ${port}.`));